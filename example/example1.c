#include <stdio.h>
#include <stdlib.h> 

typedef struct node {	

	int key; 
	int value; 
	struct node* next; 
	struct node* prev; 
}node;

typedef struct bucket_t{
	 node* head; 
	 node* tail;
	 int count; 
}bucket;

bucket* bucket;

int max=10; // Bucket의 최대 개수 !!!!!!!!!!!


// Hash Function 설정
int hashcode(int key){
	    return key%max;
}

node* get_element(node* list, int find_index);
void remove_element(int key);
//void rehash();
void init_array();



// User가 원한는 Key,Value를 삽입

void insert(int key, int value){
        
	    // 어떤 Index로 들어가야 하나..

	    int index = hashcode(key);

		// item = newNode !!!!!!!!!!!!!
		node* list = (node*)bucket[index].head;
		node* item = (node*)malloc(sizeof(node));

		item->key =key;
		item->value = value;
		item->next =NULL;

		
		// 넣고자 하는 Index에 값이 있는 경우

		if (list ==NULL){
			printf("Inserting %d(key) and %d(value)\n",key,value);
			bucket[index].head=item;
			bucket[index].tail=item;		
			bucket->count++;
		}	
		else{
			int find_index = find(list,key);

			if(find_index==-1)
			{
				bucket[index].tail->next=item;
				bucket[index].tail=item;
				bucket->count++;
			}
			else{
				node *element = get_element(list,find_index);
				element->value=value;
			}
		}

}

int find(node *list, int key)
{
	int retval = 0;
	node *temp = list;
	while (temp != NULL) 
	{
		if (temp->key == key)
		{
			return retval;
		}
		temp = temp->next;
		retval++;
	}
	return -1;
}

node* get_element(node *list, int find_index)
{
		int i = 0;
		 node *temp = list;
		 while (i != find_index) 
		 {
			 temp = temp->next;
			 i++;	
		 }
		 return temp;
}

void display()
{
		int i = 0;
		for (i = 0; i < max; i++)
		{
			node *temp = bucket[i].head;
			if (temp == NULL) 
			{
				printf("array[%d] has no elements\n", i);
			}
			else 
			{
				printf("array[%d] has elements-: ", i);
				while (temp != NULL)
				{
					printf("key= %d  value= %d\t", temp->key, temp->value);
					temp = temp->next;
				}
				printf("\n");
			}
			
		}
}

void init_array()
{
		int i = 0;
		for (i = 0; i < max; i++)
		{
			bucket[i].head = NULL;
			bucket[i].tail = NULL;	
		}
}

void main() 
{
		int choice, key, value, n, c;
		
		bucket =(bucket*)malloc(max * sizeof(bucket*));
					
		init_array();
		
		do {
		
			
			printf("Implementation of Hash Table in C chaining with Singly Linked List \n\n");
			
			printf("MENU-: \n1.Inserting item in the Hash Table"
			
					"\n2.Removing item from the Hash Table"
					
					
					"\n3.Check the size of Hash Table" 
					
					"\n4.To display a Hash Table"
					
					
					"\n\n Please enter your choice -: ");
					
			scanf("%d", &choice);
			
			switch(choice) 
			
			
			{
				case 1:
					
					printf("Inserting element in Hash Table\n");
					
					printf("Enter key and value-:\t");
					
					scanf("%d %d", &key, &value);
					
					
					insert(key, value);
							
					break;
			
				default:
			 
			
					printf("Wrong Input\n");
						

						   		}
		 
				printf("\nDo you want to continue-:(press 1 for yes)\t");
						scanf("%d", &c);
						 
							}while(c == 1);
 
	getch();
	 
	}


/*
 *
void remove_key(int key){
	    int hashIndex = hashFunction(key);
		int flag = 0;
		node* node;
		node = hashTable[hashIndex].head;
		while (node != NULL) 
		{
			if (node->key == key){
				if (node == hashTable[hashIndex].head){
					node->next->previous = NULL; 
					hashTable[hashIndex].head = node->next; 
				}
				else{
					node->previous->next = node->next; 
					node->next->previous = node->previous; 	
				}
				hashTable[hashIndex].count--;
				free(node);
				flag = 1;	
			}
			node = node->next;	
		}
		if (flag == 1){ 
			printf("\n [ %d ] 이/가 삭제되었습니다. \n", key);	
		}
		else{ 		
			printf("\n [ %d ] 이/가 존재하지 않아 삭제하지 못했습니다.\n", key);
		}
}



void search(int key){
	int hashIndex = hashFunction(key);
	node* node = hashTable[hashIndex].head;
	int flag = 0;
	while (node != NULL)
	{
		if (node->key == key){
			flag = 1;
			break;
		}
		node = node->next;
	}
	if (flag == 1){
		printf("\n 키는 [ %d ], 값은 [ %d ] 입니다. \n", node->key, node->value);
	}
	else{
		printf("\n 존재하지 않은 키는 찾을 수 없습니다. \n");
	}
}


void display(){

	    node* iterator;
		   
		printf("\n========= display start ========= \n");
		
		for (int i = 0; i<BUCKET_SIZE; i++){
		
			iterator = hashTable[i].head;
			
			printf("Bucket[%d] : ", i);
			
			while (iterator != NULL)
			
			
			{
			
				printf("(key : %d, val : %d)  <-> ", iterator->key, iterator->value);
				
				iterator = iterator->next;
				
			}
			
			printf("\n");
			
		
		}
		
		printf("\n========= display complete ========= \n");
}



int main(){
	hashTable = (bucket *)malloc(BUCKET_SIZE * sizeof(bucket));
	for (int i=0; i < 16; i++){
		add(i, 10*i);	
	}
	add(21, 210);
	add(31, 310);
	add(41, 410);
	display();
	remove_key(31);
	remove_key(11);
	remove_key(51);
	display();
	search(11);	
	search(1);
}



*/
