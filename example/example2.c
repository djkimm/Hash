#include "example2.h"

//int BUCKET_SIZE=10;

// Hash Function 설정

void init()
{
	int i=0;
	for(i=0; i<BUCKET_SIZE;i++)
	{
		hashTable[hashIndex].head =NULL;
	}
}

int hashFunction(int key){
	    return key%BUCKET_SIZE;
	    }

// User가 원한는 Key,Value를 삽입

void add(int key, int value){
        
	    // 어떤 Index로 들어가야 하나..

	    int hashIndex = hashFunction(key);

		node* newNode = (node*)malloc(sizeof(node));

		newNode->key=key;
		newNode->value=value;
		newNode->next=NULL;
		newNode->previous=NULL;

		// 넣고자 하는 Index에 값이 있는 경우

		if (hashTable[hashIndex].count == 0){
			hashTable[hashIndex].count = 1;
			
			hashTable[hashIndex].head = newNode; // head를 교체
		}
		else{
			hashTable[hashIndex].head->previous = newNode; // 추가
			
			newNode->next = hashTable[hashIndex].head;
			
			hashTable[hashIndex].head = newNode;
			
			hashTable[hashIndex].count++;
		}
}

void remove_key(int key){
	    int hashIndex = hashFunction(key);
		int flag = 0;
		node* node;
		node = hashTable[hashIndex].head;
		while (node != NULL) 
		{
			if (node->key == key){
				if (node == hashTable[hashIndex].head){
					node->next->previous = NULL; 
					hashTable[hashIndex].head = node->next; 
				}
				else{
					node->previous->next = node->next; 
					node->next->previous = node->previous; 	
				}
				hashTable[hashIndex].count--;
				free(node);
				flag = 1;	
			}
			node = node->next;	
		}
		if (flag == 1){ 
			printf("\n [ %d ] 이/가 삭제되었습니다. \n", key);	
		}
		else{ 		
			printf("\n [ %d ] 이/가 존재하지 않아 삭제하지 못했습니다.\n", key);
		}
}



void search(int key){
	int hashIndex = hashFunction(key);
	node* node = hashTable[hashIndex].head;
	int flag = 0;
	while (node != NULL)
	{
		if (node->key == key){
			flag = 1;
			break;
		}
		node = node->next;
	}
	if (flag == 1){
		printf("\n 키는 [ %d ], 값은 [ %d ] 입니다. \n", node->key, node->value);
	}
	else{
		printf("\n 존재하지 않은 키는 찾을 수 없습니다. \n");
	}
}

void display(){

	    node* iterator;
		   
		printf("\n========= display start ========= \n");
		
		for (int i = 0; i<BUCKET_SIZE; i++){
		
			iterator = hashTable[i].head;
			
			printf("Bucket[%d] : ", i);
			
			while (iterator != NULL)
			
			
			{
			
				printf("(key : %d, val : %d)  <-> ", iterator->key, iterator->value);
				
				iterator = iterator->next;
				
			}
			
			printf("\n");
			
		
		}
		
		printf("\n========= display complete ========= \n");
}


