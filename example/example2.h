#ifndef __EXAMPLE_H__
#define __EXAMPLE_H__

#include <stdio.h>
#include <stdlib.h> 

struct bucket* hashTable; 

int BUCKET_SIZE=0;

typedef struct node {	
	int key; 
	int value; 
	struct node* next; 
	struct node* previous; 
}node;

typedef struct bucket{
	struct node* head; 	
	int count; 
}bucket;

node* createNode(int key,int value);

int hashFunction(int key);

void add(int key, int value);

void remove_key(int key);

void search(int key);

void display();

#endif

