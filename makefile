CC = gcc
CFLAGS = -g -Wall
TARGET = test.out
OBJECTS = Person.o Table.o dbLinkedList.o chainedtablemain.o 

${TARGET} : ${OBJECTS}
	gcc -o ${TARGET} ${OBJECTS}

dbLinkedList.o : dbLinkedList.c
	 gcc -c dbLinkedList.c

Person.o : Person.c
	 gcc -c Person.c
Table.o : Table.c
	gcc -c Table.c
chainedtablemain.o : chainedtablemain.c
	gcc -c chainedtablemain.c
		
