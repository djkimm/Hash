#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Person.h"

//테이블에 저장될 값

// ssn(주민번호), name(이름), address(주소)

// ssn을 key로 결정! -> key를 별도로 추출하기 위한 함수
int GetSSN(Person* p){
	return p->ssn;
}


void ShowPerInfo(Person* p){

	printf("주민등록번호: %d\n",p->ssn);
	printf("이름: %s\n",p->name);
	printf("주소: %s\n",p->address);

}

// Person 구조체 변수의 생성 및 초기
Person* MakePersonData(int ssn, char* name, char* address)
{
	Person* newPerson =(Person*)malloc(sizeof(Person));
	newPerson->ssn=ssn;
	strcpy(newPerson->name,name);
	strcpy(newPerson->address,address);
	return newPerson;
}

