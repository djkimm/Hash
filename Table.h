#ifndef __TABLE_H__
#define __TABLE_H__

#include "Slot.h"
#include "dbLinkedList.h"
#define MAX_TBL	100

typedef int (*HashFunc)(key k);

typedef struct _table
{
	List tbl[MAX_TBL];
	HashFunc *hash;
}Table;

void TableInit(Table *table, HashFunc* hash);
void TableInsert(Table *table, Key k, Person p);
Person TableDelete(Table* table, Key k);
Person TableSearch(Table* table, Key k);

#endif
