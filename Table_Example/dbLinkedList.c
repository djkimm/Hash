#include "dbLinkedList.h"

//=============================================================
//List 초기화 함수
//============================================================
dl_List* Init()
{  
	dl_List* list = (dl_List*)malloc(sizeof(dl_List));

	if(list == NULL){
		printf("Malloc Fail\n");
		return NULL;
	}

	list -> head = NULL;
	
	list -> tail = NULL;

	list -> count = 0;

	list -> comp = NULL;

	printf("----------------------------------\n");
	
	printf("Double Linked List start\n");
	
	printf("----------------------------------\n");

	return list;
}


//==================노드 추가 함수===========================
//================ 1. Head에 추가 ===========================

void add_First_Node(dl_List* list, int data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	
	newNode -> data = data;
	
	if(list -> head == NULL){

		//list -> head  = list ->tail = newNode;

		//newNode -> next = list->tail;
		//list ->head=newNode;
		
		newNode->next = NULL;
		newNode->prev = NULL;

		list->head = newNode; //prev추가!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		newNode -> next = list ->tail;

		printf(" -> data added(First)......(%d)\n", newNode->data);
		printf("\n");
	}
	else
	{
		list -> head ->prev = newNode->prev; //prev 추가!!!!!!!!!!!!!!!!!!!!!!!//앞에 prev삭제함!

		newNode -> next = list -> head;

		list -> head = newNode;

		printf(" -> data added(First)......(%d)\n", newNode->data);
		printf("\n");

	}
		list -> count++ ;
}

//================== 2. Tail에추가==========================

void add_Node(dl_List* list, int data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));

	newNode -> data = data;

	if(list -> tail  == NULL){
		
		//list-> head = list -> tail = newNode;

		//newNode -> next = list->tail;

		//list -> head = newNode;	
		newNode->next=NULL;
		newNode->prev=NULL;

		list->head=newNode;
		newNode->next=list->tail;

		printf(" -> data added(First)......(%d)\n", newNode->data);
		printf("\n");
	}
	else{
		//newNode -> prev = list -> tail;

		//newNode -> next = list -> tail;

		//list-> tail -> next = newNode;

		//list -> tail = newNode;
		
		list->tail=newNode;
		newNode->next=list->tail;
		newNode->prev=list->tail;




		printf(" -> data added(Last)......(%d)\n", newNode -> data);
		printf("\n");

	}
		list -> count ++;
}

//======== 3. 삽입하고 싶은 부분에 추가=====================

void insert_Node(dl_List* list, int n, int data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	Node* cur;

	newNode -> data = data;

	int list_Size = list->count;

//	printf("\n삽입할 노드 위치:");

//	scanf("%d",&n);

	if(n<1 || n>list_Size+1){
		printf("Wrong Position Selected\n");
		printf("\n");
		return;
	}

	if(n==1){
		add_First_Node(list,data);
	}
	else if(n == list_Size+1)
	{
		add_Node(list,data);
	}
	else// if(1<n<=list_Size)
	{
		int number = n-1;
		cur = list->head;
		for(int i=0; i<number-1;i++)
		{
			cur = cur->next;
		}
		newNode->prev=cur;
		newNode->next=cur->next;
		cur->next=newNode;
		newNode->next->prev=newNode;

		printf(" -> data added(%d)Node......(%d)\n",n, newNode -> data);
		printf("\n");
		list->count++;
	}
//	else if(n>list_Size){
//		return;
//	}
//	else{
//		printf("33\n");
//	}


/*	
	if(n <= list->count)
	{
		if(n==1)
		{
			add_First_Node(list,data);
		}

	//	else if(n == list_Size-1)
	//		add_Node(list,data);

		else// if(1<n<=list->count)
		{
			int number = n-1;
		
			cur = list->head;

			for(int i=0;i<number-1;i++)
			{
				cur = cur->next;
			}
		
			newNode->prev = cur;

			newNode->next = cur->next;

			cur->next=newNode;
			newNode->next->prev=newNode;

			printf(" -> data added(%d)Node......(%d)\n",n, newNode -> data);
			printf("\n");

			list->count++;
		}
	
	}
	else{
		printf("Wrong Position Selected\n");
	}
*/
}
//====================노드 삭제 함수 ==================

//===============1. Head 노드 삭제 함수===============

void delete_First_Node(dl_List*list)
{
	Node* cur;
	Node* tmp;

	tmp = list -> head;

	if(tmp == NULL){
		printf("List is empty\n\n");
		return;
	}
	
	if(list->count ==1)
	{
		
		printf(" -> data deleted(First)......(%d)\n\n", tmp -> data);
		list->head =NULL;
		list->tail =NULL;
		free(tmp);
		list->count--;
	}

	else{
		cur = list -> head -> next;

		cur -> prev = list -> head -> next;

		list -> head = cur;

		printf(" -> data deleted(First)......(%d)\n", tmp -> data);

		printf("\n");

		free(tmp);

		list -> count--;
	}
}

//================2. 뒤에서 부터 노드 삭제================

void delete_Last_Node(dl_List* list)
{
	Node* cur;
	Node* tmp;

	tmp = list -> tail;

	if(tmp == NULL){
		
		printf("List is empty\n\n");
		return;
	}

	if(list->count==1)
	{	
		printf(" -> data deleted(First)......(%d)\n", tmp -> data);
		list->head=NULL;
		list->tail=NULL;
		free(tmp);
		list->count--;
	}
	else{

		cur = list -> tail -> prev;

		cur -> next = list -> tail;

		list -> tail = cur;

		printf(" -> data deleted(Last)......(%d)\n",tmp -> data);
		printf("\n");

		free(tmp);

		list -> count --;
	}
}

//=============== 3. 삭제하고 싶은 노드 삭제===================

void delete_Node(dl_List* list, int n)
{
	Node* cur;
	Node* tmp;

	int last_Size = list -> count;

//	printf("\n삭제할 노드 위치:");

//	scanf("%d",&n);
	
	if(n<1 || n>last_Size){
		printf("Wrong Position Selected..Out Of List\n");
		printf("Current List Range: (%d)\n\n",list->count);
		return;
	}
	if(0<  n <= list->count){

		if(list-> head ==NULL){
			puts("List is empty");
		}

		else if (n ==1){
			delete_First_Node(list);
		}

		else if (n ==last_Size){
			delete_Last_Node(list);
		}

		else{

			int number = n-1;

			cur = list ->head;

			for(int i=0; i<n-1; i++)
				cur = cur -> next;

			tmp =cur;

			cur -> prev -> next = cur -> next;

			cur -> next -> prev = cur -> next;

			printf(" -> data deleted(%d)Node......(%d)\n",n,tmp->data);
			printf("\n");

			free(tmp);

			list -> count --;
		}
	}
	else{
		puts("you entered wrong");
	}
}

//=========== 데이터 탐색 함수=================//
/*
void node_Search(dl_List* list, int number)
{
	Node* cur = list -> head;

	if(list-> head  == NULL)
	{
		printf("\n저장된데이터가 없습니다.\n");
	}
	else 
	{	
		while(cur!=NULL)
		{
			if(cur -> data == number)
			{
				printf("저장된 %d 데이터를 출력하였습니다.\n\n",number);
				cur =cur->next;
			}
			return;
		}

		if(cur==NULL){

			printf("찾는 값이 없습니다.\n\n");

			return;
		}
	
	}

}
*/
void node_Search(dl_List* list, int number)
{
	Node* cur = list->head;
	int node_Number = list->count;

	if(list->head ==NULL)
	{
		printf("\n저장된 데이터가 없습니다.\n");
	}
	else
	{
		for(int i=0;i<node_Number;i++)
		{
			if(cur->data ==number)
			{
				printf("저장된 %d의 데이터를 출력하였습니다.\n\n",number);
			}
		cur = cur->next;
		if(cur == NULL){
			//printf("저장된 %d의 데이터를 출력하였습니다.\n\n",number);
			printf("더 이상 찾는 값이 없습니다.\n\n");
			return;
		}
	
		}
		
	}
		

}

void add_Compare_Node(dl_List* list, int data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));

	Node* cur = list -> head;

	newNode -> data = data;

	
	if(cur -> next == NULL && list -> comp(data, cur -> next -> data) !=0)

	while(cur -> next != NULL && list -> comp(data, cur -> next -> data) !=0)
	{
		cur = cur -> next;
	}

	newNode -> prev = cur;

	newNode -> next = cur -> next;

	cur -> next = newNode;

	newNode -> next -> prev = newNode;

	printf(" -> data added(Insert)......(%d)\n",newNode->data);
	printf("\n");

	list -> count ++;
}

void setSortRule(dl_List* list, int(*comp)(int data1, int data2))
{
	list -> comp =comp;
}

int list_Size(dl_List* list){

	return list -> count;
}

void all_Node_Delete(dl_List* list)
{
	Node* tmp;
	Node* cur;

	cur = list-> head;

	if(list->head == NULL)
	{
		printf("List is empty now----> 초기화할 내용이 없음.\n\n");
	}

	else{
	printf("--------------------------------\n");
	printf("Double Linked List Delete\n");
	printf("--------------------------------\n");
	}

	while(cur != list->tail)
	{
		tmp = cur -> next;
		free(cur);
		cur = tmp;
	}
	free(cur);

	list -> head = NULL;

	list -> tail = NULL;

	list -> count =0;
}
void print_List(dl_List *list){ 
	Node *cur;
				               
	cur = list->head;
	
	if(cur == NULL){

		puts("List is empty.\n");
		
	}
				                
	else{
		
		int n = 1;

		while(n <= list->count){

			printf("List[%d] = %d.\n", n, cur->data);
			
			cur = cur->next; 
			
			n++;
			
		}
	}
	printf("현재 데이터의 수:%d\n",list_Size(list));
	printf("\n");

}
