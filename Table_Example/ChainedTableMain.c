#include <stdio.h>
#include <stdlib.h>
#include "Person.h"
#include "Table.h"

int MyHashFunc(int k)
{
	return k%100;
}

int main(void)
{
	Table myTbl;
	Person* np;
	Person* sp;
	Person* rp;
	
	TableInit(&myTbl, MyHashFunc);

	np = MakePersonData(900254, "LEE","sEOUL");
	TableInsert(&myTbl,GetSSN(np),np);

	return 0;
}
