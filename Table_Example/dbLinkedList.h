#ifndef __DB_LINKED_LIST_H__
#define __db_linked_list_h__

#define TRUE 1
#define FALSE 0
#include<stdio.h>
#include<stdlib.h>

#include "Slot.h"

typedef Slot Data;

typedef struct _node_s{
	       
	Data  data;
	
	struct _node_s* next;

	struct _node_s* prev;

}Node;

typedef struct _dbLinked_List_s{

	Node* head;
	
	Node* tail;
	
	int count;

	int (*comp)(int data1, int data2);

}dl_List;



dl_List *Init();

void add_First_Node(dl_List* list, int data);
/*
void add_Node(dl_List* list, int data);

void insert_Node(dl_List* list, int n, int data);

void delete_First_Node(dl_List* list);

void delete_Last_Node(dl_List* list);

void delete_Node(dl_List* list, int n);

void node_Search(dl_List* list, int number);

void setSortRule(dl_List* list, int(*comp)(int data1, int data2));

void add_Compare_Node(dl_List* list, int data);

int list_Size(dl_List* list);

void all_Node_Delete(dl_List* list);

void print_List(dl_List* list);
*/
#endif
