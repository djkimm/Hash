#ifndef __TABLE_H__
#define __TABLE_H__

#include "Slot.h"
#include "dbLinkedList.h"

#define MAX_TBL 100

typedef int(*HashFunc)(Key k);

typedef struct _table
{
	dl_List tbl[MAX_TBL];
	HashFunc* hash;
}Table;

void TableInit(Table* table,HashFunc* hash);
void TableInsert(Table* table, Key k,Value v);
Value TableDelete(Table* table, Key k);
Value TableSearch(Table* table, Key k);

#endif
