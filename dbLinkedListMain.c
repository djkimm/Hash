#include "dbLinkedList.h"


int whoIsPrecede(int data1, int data2)
{
	if (data1 < data2)
		return 0;
	else 
		return 1;
}

int main(void){

			int menu = 0;
			int data,n;
			int number;
			int check;

	        dl_List* list = Init();
			setSortRule(list, whoIsPrecede);
			
			void print_Menu(){

			printf("1.Add Node(Last position)\n");

			printf("2.Add Node(First Position)\n");

			printf("3.Node Insert(Want Position)\n");

			printf("4.First Node Delete\n");

			printf("5.Last Node Delete\n");

			printf("6.Node Delete(Want Position)\n");

			printf("7.Data Search\n");

			printf("8.Compare data & Insert\n");

			printf("9.List Reset \n");

			printf("10.List Print\n");

			printf("11.Menu Print\n");
			printf("12.Quit\n");
			}

			print_Menu();

			while(menu!=12){
				printf("선택:");
				check = scanf("%d",&menu);
				
				while(getchar() !='\n');
				if(check==0) {
				printf("Only Insert Int data(1~12)\n\n");
					continue;
				}
									
				switch(menu){
					case 1:
						printf("\n삽입할 원소값:");
						check=scanf("%d",&data);		

						while(getchar() !='\n');
						if(check==0){	
							printf("Invalid data.\n\n");
							continue;
						}
						add_Node(list,data);
						break;
					case 2:
						printf("\n삽입할 원소값:");
						check=scanf("%d",&data);
						
						while(getchar() !='\n');
						if(check==0){
							printf("Invalid data\n\n");
							continue;
						}
						add_First_Node(list,data);
						break;
					case 3:
						printf("\n삽입할 노드 위치:");
						check=scanf("%d",&n);
						while(getchar() != '\n');
						if(check==0){
							printf("Invalid data\n\n");
							continue;
						}
						printf("\n삽입합 원소값:");
						check=scanf("%d",&data);
						
						while(getchar() !='\n');
						if(check==0){
							printf("Invalid data\n\n");
							continue;
						}
						insert_Node(list,n,data);
						break;
					case 4:
						printf("\nNode Delete\n");
						delete_First_Node(list);
						break;
					case 5:
						printf("\n마지막 노드 제거\n");
						delete_Last_Node(list);
						break;
					case 6:
						printf("\nn번째 노드 제거:");
						check=scanf("%d",&n);
						while(getchar() != '\n');
						if(check==0){
							printf("Invalid data\n\n");
							continue;
						}
						delete_Node(list,n);
						break;
					case 7:
						printf("\n데이터 검색: ");
					    check=scanf("%d",&number);
						while(getchar() != '\n');
						if(check==0){
							printf("Invalid data\n\n");
							continue;
						}
						node_Search(list,number);
						break;
					case 8:
						printf("\n비교 삽입할 데이터:");
						check=scanf("%d",&data);
						
						while(getchar() !='\n');
						if(check==0){
							printf("Invalid data\n\n");
							continue;
						}
						add_Compare_Node(list,data);
						break;
					case 9:
						printf("\n리스트 초기화\n");
						all_Node_Delete(list);
						break;
					case 10:
						printf("\n리스트 전체 출력\n");
						print_List(list);
						break;
					case 11:
						printf("\nMenu Print\n");
						print_Menu();
					case 12:
						break;
						
					default:
						printf("\n잘못 선택\n\n");
			
				}
			}

		
		/*	
			add_Node(list,3);
			print_List(list);

			add_Node(list,5);

			add_First_Node(list,1);

			delete_First_Node(list);

			//add_Compare_Node(list,2);

			insert_Node(list,1,322);
			
			insert_Node(list,2,33);
			print_List(list);

			all_Node_Delete(list);
			print_List(list);
		
			//delete_Node(list,2);
			add_First_Node(list,7);
								
			print_List(list);

			add_Node(list,5);
			add_First_Node(list,5);
			print_List(list);
			add_Compare_Node(list,6);
			node_Search(list,5);
			node_Search(list,4);
			print_List(list);

			print_List(list);

			add_Node(list,7);

			print_List(list);

			insert_Node(list,2,184);

			add_Node(list,8);

			print_List(list);

			add_Node(list,9);

			print_List(list);
			*/
}
