#include<stdio.h>
#include<stdlib.h>

#include"Table.h"
#include"dbLinkedList.h"

void TableInit(Table* table, HashFunc* hash)
{
	int i;

	for(i=0; i<MAX_TBL; i++)
	{
		tbl[i].head=NULL;
		tbl[i].tail=NULL;
	}
	table->hash=hash;
}

void TableInsert(Table* table,Key k,Person p)
{
	int hash = table ->hash(k);
	table->tbl[hash].Person=p;
	table->tbl[hash].Key=k;

}


